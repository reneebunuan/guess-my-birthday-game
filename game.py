#Ask for the person's name
name=input("Hello, what is your name? ")
print("Hello", name, "I am going to guess your birthday!")


#GUESS 1-5

for guess_number in range(1, 6):
    from random import randint
    guess_month=randint(1,12)
    guess_year=randint(1924,2004)

    print(name, "were you born in", guess_month, "/", guess_year, "?")
    response=input("yes or no?" )

    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 5:
        print("I give up! I have other things to do. Goodbye!")
    else:
        print("Let me try again.")   
